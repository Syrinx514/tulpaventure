How to get this program working:

WINDOWS USERS:

Install Python (32 bit) http://python.org/ftp/python/3.3.2/python-3.3.2.msi

Install Python (64 bit) http://python.org/ftp/python/3.3.2/python-3.3.2.amd64.msi

Open the program (tulpaventure.py) with IDLE (If this does not work contact me.)

Perhaps using Cygwin can work too.

MAC OSX USERS:

Install Python (32 bit) http://python.org/ftp/python/3.3.2/python-3.3.2-macosx10.5.dmg

Install Python (64 bit) http://python.org/ftp/python/3.3.2/python-3.3.2-macosx10.6.dmg

Open the program in the terminal (I cannot confirm this, if there are any OSX users who know how to open .py files please tell me how so I can update this with the correct procedure.)

LINUX USERS:

I am assuming you already have Python installed, if not, open your terminal and type

sudo apt-get install python

Alternatively, you can download this http://python.org/ftp/python/3.3.2/Python-3.3.2.tar.bz2

Next, go to the directory in which you installed the file (I am assuming you already know how to do this) and type

python tulpaventure.py

Have fun! Feedback is appreciated, and remember this is still in very early development, and more features are to come soon.
