#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2013 Syrinx <syrinx@Daedalus>
#  
#  Made by ThunderClap from tulpa.info (ThunderClap_ on r/tulpas) 
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

finish = "Please restart the program to select another option. Thanks for playing!"

print 'So you decided to create a tulpa. Aint that incredible. In this text RPG you will learn the true aspects of creating a tulpa in a fun and interactive way.'
print 'Feel free to modify the game in any way. This software is licenced under the GNU General Public License. Original code written by ThunderClap.'
print 'The idea of this game is to discuss the choices with your tulpa and further develop their sentience.'
print 'So you got 3 choices in what you want to start with.'
print '#1 Talk to your tulpa'
print '#2 Visualize your tulpa'
print '#3 Work on your tulpas personality'
print 'Select a choice by typing in 1, 2, or 3 below.'
choice = raw_input('> ')

if choice == "1":
	print 'You begin to sit down in your wonderland and talk to your tulpa. You discuss various things you did throughout your day, and or things you plan on doing in the future. Your tulpa is enlightened by this, and is interesting in what you are saying. Though you may or may not realize it, they are listening attentivly.'
	print 'Next you begin to walk around in your wonderland, discussing the various sights there are to see, maybe it is a tree, or a lake, you talk about all the different things around you. Your tulpa is enlightened by this experence, and may have possibly grown a likeing for nature. As you are walking along, you notice a fork in the road.'
	print 'On the left, the trail leads to a nearby river. On the right however, the trail leads to a lush forest. Which do you choose?'
	print 'Type 1 for the river, or 2 for the forest'
	road = raw_input('> ')

	if road == "1":
		print 'You and your tulpa decide to go to the river. You sit down at a nearby rock.'
		print 'As the sound soothes your mind, you feel a closer connection with your tulpa as you sit down together near the river.'
		print 'Eventually after relaxing at the river for some time, you two leave the river and go back to where you came from. You wave goodbye to your tulpa and end the forcing session.'
		print finish
		
	elif road == "2":
		print 'You and your tulpa decide the forest is the best option and walk over to it.'
		print 'You notice the lush green leaves on the nearby trees and even hear the sound of birds chirping in the distance.'
		print 'Your mind is now truely at ease as you relax yourself in the peaceful forest, and you feel a stronger connection with your tulpa as you spend time together exploring the forest.'
		print 'After you are finished exploring, you wave goodbye to your tulpa friend and end the session there.'
		print finish

elif choice == "2":
	print 'You decide visualizing your tulpa is the best option.'
	print 'As you immerse yourself in your wonderland, or where ever you decide to visualize your tulpa, you begin to picture them in your mind.'
	print 'You are able to point out various things about them, such as their hair, skin tone, body structure, ext.'
	print 'Although this may be difficult for you at first, you think to yourself that it is okay, because eventually you will get better at it.'
	print 'As you are visualizing your tulpa, you begin to grow tired. Do you wish to continue visualizing your tulpa in your wonderland, or do you want to end the session?'
	print 'Type 1 to continue or 2 to exit.'
	picture = raw_input('> ')

	if picture == "1":
		print 'You decide it is best to continue to imagine your tulpa in detail.'
		print 'Good choice, your tulpa is happy that you are willing to dedicate yourself to them.'
		print 'After an hour or so of forcing, you wave goodbye to your tulpa and the session ends.'
		print 'You then go waste time on the IRC and explain what you did.'
		print 'how great.'

	else:
		print 'You end the session.'
		print 'You lazy fuck now go waste time on the IRC and complain about how you cannot visualize because you decided you did not want to.'

if choice == "3":
	print 'You decide that personality is the thing that you want to work on.'
	print 'Perhaps you have a list of things to work on with personality. Do you?'
	print 'Type YES or NO'
	answer = raw_input('> ')
	
	if answer == "YES":
		print 'You begin to remember the list of personality traits you complied for your tulpa.'
		print 'You read off the traits over and over again to your tulpa, and discuss how those traits apply to them.'
		print 'Such traits may inclue their likes, dislikes, weaknesses, strengths, preferences, and more.'
		print 'After reading off the traits for quite some time, you wave goodbye to your tulpa and the session is over.'
		print finish
	
	if answer == "NO":
		print 'You did not think to create a list of tulpa traits. That is fine, though it is advisable to do so.'
		print 'You think of some traits on a whim and imagine how they apply to your tulpa, perhaps it is also good to imagine the tulpa in different situations where said traits will apply.'
		print 'After doing this for sometime, you say goodbye and end the session. Perhaps it would be a good idea to make that list now.'
		print finish
	
    

